.. mogan-specs documentation master file

==============================
OpenStack Mogan Project Plans
==============================

Specifications
==============

Specifications for the mogan project are available here. Specifications
begin life in the "approved" tree. They stay there (possibly beyond the
development cycle in which they had been approved), and once implemented,
are moved to the "implemented" tree for that development cycle.
Additionally, a "backlog" of ideas is maintained to indicate
the agreed-upon goals for the project which have no specific work being done
on them at this time.
